-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2013 at 06:00 PM
-- Server version: 5.5.31-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Cview`
--

-- --------------------------------------------------------

--
-- Table structure for table `AccesControls`
--

CREATE TABLE IF NOT EXISTS `AccesControls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Alias` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Pswrd` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Shopkeeper_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_61CC41493D7AB380` (`Shopkeeper_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `AccesControls`
--

INSERT INTO `AccesControls` (`id`, `Alias`, `Pswrd`, `Shopkeeper_id`) VALUES
(3, 'hocklo', '1d0258c2440a8d19e716292b231e3190', 33);

-- --------------------------------------------------------

--
-- Table structure for table `Brands`
--

CREATE TABLE IF NOT EXISTS `Brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Thumbnails` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_790E4102B6723DA0` (`Country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Cities`
--

CREATE TABLE IF NOT EXISTS `Cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Country_id` int(11) DEFAULT NULL,
  `Region_id` int(11) DEFAULT NULL,
  `Province_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DEF1B45DB6723DA0` (`Country_id`),
  KEY `IDX_DEF1B45D1AD783F6` (`Region_id`),
  KEY `IDX_DEF1B45D6FDA9E9D` (`Province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `Cities`
--

INSERT INTO `Cities` (`id`, `Name`, `Country_id`, `Region_id`, `Province_id`) VALUES
(8, 'Castellfullit de la Roca', NULL, NULL, 6),
(9, 'Girona', NULL, NULL, 7),
(10, 'Madrid', NULL, NULL, 8),
(11, 'Barcelona', NULL, NULL, 9),
(12, 'Esplugues de Llobregat', NULL, NULL, 9),
(13, 'Pamplona', NULL, NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `Countries`
--

CREATE TABLE IF NOT EXISTS `Countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Products`
--

CREATE TABLE IF NOT EXISTS `Products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Price` double NOT NULL,
  `Brand_id` int(11) DEFAULT NULL,
  `Shop_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4ACC380CBD83B25E` (`Brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `Products`
--

INSERT INTO `Products` (`id`, `Name`, `Description`, `Image`, `Price`, `Brand_id`, `Shop_id`) VALUES
(11, 'Alimento de prueba', 'Perrito bonito\r\n200gr.', 'second.jpg', 20, NULL, 19),
(13, 'Pan de molde', 'flajdflkñajdflkajflk', 'browser-icon-safari.png', 2, NULL, 21),
(15, 'test', 'adfafafda', 'slider_1.jpg', 20, NULL, 21),
(16, 'test', 'adfafafda', 'slider_1.jpg', 20, NULL, 21),
(17, 'test2', 'fasfadsfasfdsa', 'slider_3.jpeg', 20, NULL, 21),
(18, 'Barra de pan Integralt', 'tra', 'dessarrollador.jpg', 2, NULL, 26);

-- --------------------------------------------------------

--
-- Table structure for table `Provinces`
--

CREATE TABLE IF NOT EXISTS `Provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Country_id` int(11) DEFAULT NULL,
  `Region_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E674EF4B6723DA0` (`Country_id`),
  KEY `IDX_E674EF41AD783F6` (`Region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `Provinces`
--

INSERT INTO `Provinces` (`id`, `Name`, `Country_id`, `Region_id`) VALUES
(6, 'Girona', NULL, NULL),
(7, 'Cataluña', NULL, NULL),
(8, 'Madrid', NULL, NULL),
(9, 'Barcelona', NULL, NULL),
(10, 'Navarra', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Regions`
--

CREATE TABLE IF NOT EXISTS `Regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6DDA406FB6723DA0` (`Country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Shopkeepers`
--

CREATE TABLE IF NOT EXISTS `Shopkeepers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Cif` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `myShops` int(11) NOT NULL DEFAULT '0',
  `maxShops` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `Shopkeepers`
--

INSERT INTO `Shopkeepers` (`id`, `Cif`, `Name`, `LastName`, `Email`, `myShops`, `maxShops`) VALUES
(33, 'A41891235', 'Eduard', 'Coll Vila', 'ecollvila@gmail.es', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Shops`
--

CREATE TABLE IF NOT EXISTS `Shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'shop_not_found.png',
  `Direction` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Longitude` double NOT NULL,
  `Latitude` double NOT NULL,
  `Shopkeeper_id` int(11) DEFAULT NULL,
  `City_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E2BB48873D7AB380` (`Shopkeeper_id`),
  KEY `IDX_E2BB488744115B33` (`City_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `Shops`
--

INSERT INTO `Shops` (`id`, `Name`, `Phone`, `Description`, `Image`, `Direction`, `Longitude`, `Latitude`, `Shopkeeper_id`, `City_id`) VALUES
(31, 'Test not found', '972294536', 'Test foto botiga not found', 'shop_not_found.png', 'Travesía Acella, Pamplona, España', -1.6633962999999312, 42.8066883, 33, 13);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AccesControls`
--
ALTER TABLE `AccesControls`
  ADD CONSTRAINT `FK_61CC41493D7AB380` FOREIGN KEY (`Shopkeeper_id`) REFERENCES `Shopkeepers` (`id`);

--
-- Constraints for table `Brands`
--
ALTER TABLE `Brands`
  ADD CONSTRAINT `FK_790E4102B6723DA0` FOREIGN KEY (`Country_id`) REFERENCES `Countries` (`id`);

--
-- Constraints for table `Cities`
--
ALTER TABLE `Cities`
  ADD CONSTRAINT `FK_DEF1B45D1AD783F6` FOREIGN KEY (`Region_id`) REFERENCES `Regions` (`id`),
  ADD CONSTRAINT `FK_DEF1B45D6FDA9E9D` FOREIGN KEY (`Province_id`) REFERENCES `Provinces` (`id`),
  ADD CONSTRAINT `FK_DEF1B45DB6723DA0` FOREIGN KEY (`Country_id`) REFERENCES `Countries` (`id`);

--
-- Constraints for table `Products`
--
ALTER TABLE `Products`
  ADD CONSTRAINT `FK_4ACC380CBD83B25E` FOREIGN KEY (`Brand_id`) REFERENCES `Brands` (`id`);

--
-- Constraints for table `Provinces`
--
ALTER TABLE `Provinces`
  ADD CONSTRAINT `FK_E674EF41AD783F6` FOREIGN KEY (`Region_id`) REFERENCES `Regions` (`id`),
  ADD CONSTRAINT `FK_E674EF4B6723DA0` FOREIGN KEY (`Country_id`) REFERENCES `Countries` (`id`);

--
-- Constraints for table `Regions`
--
ALTER TABLE `Regions`
  ADD CONSTRAINT `FK_6DDA406FB6723DA0` FOREIGN KEY (`Country_id`) REFERENCES `Countries` (`id`);

--
-- Constraints for table `Shops`
--
ALTER TABLE `Shops`
  ADD CONSTRAINT `FK_E2BB48873D7AB380` FOREIGN KEY (`Shopkeeper_id`) REFERENCES `Shopkeepers` (`id`),
  ADD CONSTRAINT `FK_E2BB488744115B33` FOREIGN KEY (`City_id`) REFERENCES `Cities` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
