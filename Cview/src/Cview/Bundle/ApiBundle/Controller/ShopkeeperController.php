<?php

namespace Cview\Bundle\ApiBundle\Controller;

//use Symfony\Component\BrowserKit\Response;

use Cview\Bundle\ApiBundle\Entity\Shopkeeper;

use Symfony\Component\DependencyInjection\SimpleXMLElement;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Cview\Bundle\PrincipalBundle\Entity\Shopkeepers;

class ShopkeeperController extends Controller{
	/**
	 * Show All Shopkeepers
	 */
	public function allAction(){
		$em = $this->getDoctrine()->getManager();
		$q = $em -> createQuery('select s from Cview\Bundle\PrincipalBundle\Entity\Shopkeepers s');
		$shopkeepers = $q->getArrayResult();

        if (!$shopkeepers) {
            throw $this->createNotFoundException('Unable to find Shopkeeper.');
        }

        $response = new Response(json_encode(array('shopkeepers' => $shopkeepers)));
        return $response;
	}

	/**
	 * Show Shopkeeper for id
	 */
	public function getAction($id){

		$conn = $this->get('database_connection');
		$shopkeeper = $conn->fetchAll('SELECT * FROM Shopkeepers WHERE id='.$id);
        
        if (!$shopkeeper) {
            //$error = $this->createNotFoundException('Unable to find Shopkeeper.');
            $error = 'Unable to find Shopkeeper.';
        	return $this->$error;
        }
        
        $response = new Response(json_encode(array('shopkeeper' => $shopkeeper)));
        return $response;
	}
	/**
	 * Show Shopkeeper for cif
	 */
	public function getforcifAction($cif){
		/*
			$q = $em->createQuery('select s from Cview\Bundle\PrincipalBundle\Entity\Shopkeepers s WHERE s.id = 0');
		//$q -> setParameter('id', $id);
		$shopkeepers = $q->getArrayResult();
		*/
		$em =$this->getDoctrine()->getManager();
		$shopkeeper = $em ->getRepository('CviewPrincipalBundle:Shopkeepers')
						  ->findOneBy(array('Cif'=>$cif));

		if (!$shopkeeper) {
			//$error = $this->createNotFoundException('Unable to find Shopkeeper.');
			$error = 'Unable to find Shopkeeper.';
			return $this->$error;
		}
	
		//$response = new Response(json_encode(array('shopkeeper' => $shopkeeper)));
		return $shopkeeper;
	}
	/**
	 * GetName Shopkeeper for id
	 * @return username
	 */
	public function getNameAction(){
		$loged = (isset($_COOKIE['Shopkeeper'])) ? true : false;
		if($loged){
			$em = $this->getDoctrine()->getManager();
			$cif = $_COOKIE['Shopkeeper'];
			$cif= base64_decode($cif);
			$cif = substr($cif, 0,4).substr($cif,8,12);
			$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));

			return new Response($shopkeeper->getName());
		}
	}
	public function getMyShopsAction(){
		$loged = (isset($_COOKIE['Shopkeeper'])) ? true : false;
		if($loged){
			$em = $this->getDoctrine()->getManager();
			$cif = $_COOKIE['Shopkeeper'];
			$cif= base64_decode($cif);
			$cif = substr($cif, 0,4).substr($cif,8,12);
			$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
	
			return new Response($shopkeeper->getMyShops());
		}
	}
	public function getMaxShopsAction(){
		$loged = (isset($_COOKIE['Shopkeeper'])) ? true : false;
		if($loged){
			$em = $this->getDoctrine()->getManager();
			$cif = $_COOKIE['Shopkeeper'];
			$cif= base64_decode($cif);
			$cif = substr($cif, 0,4).substr($cif,8,12);
			$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
	
			return new Response($shopkeeper->getMaxShops());
		}
	}
	/**
	 * Insert Shopkeeper
	 */
	public function insertAction($cif,$name,$lastname,$email){
		$shopkeeper = new Shopkeepers();
		
		$shopkeeper->setCif($cif);
		$shopkeeper->setName($name);
		$shopkeeper->setLastName($lastname);
		$shopkeeper->setEmail($email);
		$shopkeeper->setMyShops(0);
		$shopkeeper->setMaxShops(2);
		
		$em = $this->getDoctrine()->getManager();
		$em -> persist($shopkeeper);
		$em -> flush();
	}
	/**
	 * Check Shopkeeper
	 */
	public function checkAction($cif){
		$conn = $this->get('database_connection');
		$shopkeeper = $conn->fetchAll('SELECT * FROM Shopkeepers WHERE Cif="'.$cif.'"');
		return (!$shopkeeper) ? true : false;
	}
	

}