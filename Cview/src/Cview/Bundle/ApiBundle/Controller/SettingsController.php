<?php
namespace Cview\Bundle\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SettingsController extends Controller{
	/**
	 * Update Settings
	 */
	public function updateAction(){
		$name = $_POST['name'];
		$cif = $_POST['Cif'];
		$email = $_POST['Email'];
		$alias = $_POST['Alias'];
		if(isset($_POST['chckPswrd'])){
			$check = $_POST['chckPswrd'];
		}else{$check = false;}
		$pswrd = $_POST['pswrd'];
	
		$em = $this->getDoctrine()->getManager();
		$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
		$acc= $em->getRepository('CviewPrincipalBundle:AccesControls')->findOneBy(array('Shopkeeper' => $shopkeeper,'Pswrd' => md5($pswrd)));
		if($acc){
			if($check){
				$pswrd1 = $_POST['pswrd1'];
				$pswrd2 = $_POST['pswrd2'];
				if(strlen($pswrd1)>4 && strlen($pswrd1)<11 && strlen($pswrd2)>4 && strlen($pswrd2) <11 &&$pswrd1==$pswrd2){
					$old_email = $shopkeeper->getEmail();
					if($email!=$old_email){
						//Si ha modificado su email,comprovar el nuevo
						if(self::isValidEmail($email)){
							$old_alias = $acc->getAlias();
							if($alias!=$old_alias){
								if($alias>4 && $alias<11){
									$chckAlias = $em->getRepository('CviewPrincipalBundle:AccesControls')->findOneBy(array('Alias' => $alias));
									if($chckAlias){
										$status = 3;
										return $this->render(
												'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
												array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
														'email' => $email, 'Alias' => $alias, 'status' => $status));
									}else{
										$shopkeeper->setEmail($email);
										$acc->setPswrd(md5($pswrd2));
										$acc->setAlias($alias);
										$em->persist($shopkeeper);
										$em->persist($acc);
										$em->flush();
										
										$status = 4;
										return $this->render(
												'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
												array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
														'email' => $email, 'Alias' => $alias, 'status' => $status));
									}
								}else{
									$status = 5;
									return $this->render(
											'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
											array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
													'email' => $email, 'Alias' => $alias, 'status' => $status));
								}
							}else{
									$shopkeeper->setEmail($email);
									$acc->setPswrd(md5($pswrd2));
									$em->persist($shopkeeper);
									$em->persist($acc);
									$em->flush();
									
									$status = 4;
									return $this->render(
											'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
											array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
													'email' => $email, 'Alias' => $alias, 'status' => $status));
								}
						}else{
							$status = 2;
							return $this->render(
									'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
									array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
											'email' => $email, 'Alias' => $alias, 'status' => $status));
						}
					}else{
						$acc->setPswrd(md5($pswrd2));
						$em->persist($acc);
						$em->flush();
							
						$status = 4;
						return $this->render(
								'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
								array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
										'email' => $email, 'Alias' => $alias, 'status' => $status));
					}
				}else{
					$status = 1;
					return $this->render(
		    			'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
		    			array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
		    				  'email' => $email, 'Alias' => $alias, 'status' => $status));
				}
			}else{
				$old_email = $shopkeeper->getEmail();
				if($email!=$old_email){
					//Si ha modificado su email,comprovar el nuevo
					if(self::isValidEmail($email)){
						$old_alias = $acc->getAlias();
						if($alias!=$old_alias){
							if(strlen($alias)>4 && strlen($alias)<11){
								$chckAlias = $em->getRepository('CviewPrincipalBundle:AccesControls')->findOneBy(array('Alias' => $alias));
								if($chckAlias){
									$status = 3;
									return $this->render(
											'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
											array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
													'email' => $email, 'Alias' => $alias, 'status' => $status));
								}else{
									$shopkeeper->setEmail($email);
									$acc->setAlias($alias);
									$em->persist($shopkeeper);
									$em->persist($acc);
									$em->flush();
				
									$status = 4;
									return $this->render(
											'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
											array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
													'email' => $email, 'Alias' => $alias, 'status' => $status));
								}
							}else{
								$status = 5;
								return $this->render(
										'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
										array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
												'email' => $email, 'Alias' => $alias, 'status' => $status));
							}
						}else{
							$shopkeeper->setEmail($email);
							$em->persist($shopkeeper);
							$em->flush();
								
							$status = 4;
							return $this->render(
									'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
									array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
											'email' => $email, 'Alias' => $alias, 'status' => $status));
						}
					}else{
						$status = 2;
						return $this->render(
								'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
								array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
										'email' => $email, 'Alias' => $alias, 'status' => $status));
					}
				}else{
					$old_alias = $acc->getAlias();
					if($alias!=$old_alias){
						if(strlen($alias)>4 && strlen($alias)<11){
							$chckAlias = $em->getRepository('CviewPrincipalBundle:AccesControls')->findOneBy(array('Alias' => $alias));
							if($chckAlias){
								$status = 3;
								return $this->render(
										'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
										array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
												'email' => $email, 'Alias' => $alias, 'status' => $status));
							}else{
								$shopkeeper->setEmail($email);
								$acc->setAlias($alias);
								$em->persist($shopkeeper);
								$em->persist($acc);
								$em->flush();
			
								$status = 4;
								return $this->render(
										'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
										array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
												'email' => $email, 'Alias' => $alias, 'status' => $status));
							}
						}else{
							$status = 5;
							return $this->render(
									'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
									array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
											'email' => $email, 'Alias' => $alias, 'status' => $status));
						}
					}else{
						$status = 6;
						return $this->render(
								'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
								array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
										'email' => $email, 'Alias' => $alias, 'status' => $status));
					}
				}
			}
		}else{
			$status = 0;
			/*
			return new RedirectResponse(
				$this->generateUrl(
					'CviewPrincipalBundle:UserControl:modifyUser.html.twig',
					array('name' => $name, 'cif' => $cif, 'email' => $email, 'Alias' => $alias, 'status' => $status)
				)
			);
			*/
			return $this->render(
    			'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
    			array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
    				  'email' => $email, 'Alias' => $alias, 'status' => $status));
		}
	}
	
	function isValidEmail($email){
		//Si el email es correcto devuelve true
		//En caso contrario devuelve logicamente false ^^
		return preg_match("/^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$/", $email);
	}
}
