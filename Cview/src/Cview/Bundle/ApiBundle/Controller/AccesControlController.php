<?php

namespace Cview\Bundle\ApiBundle\Controller;

//use Symfony\Component\BrowserKit\Response;


use Symfony\Component\DependencyInjection\SimpleXMLElement;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Cview\Bundle\PrincipalBundle\Entity\AccesControls;
use Cview\Bundle\PrincipalBundle\Entity\Shopkeepers;
use Cview\Bundle\ApiBundle\Controller\ShopkeeperController;


class AccesControlController extends Controller{
	/**
	 * Insert Shopkeeper
	 */
	public function insertAction($fkshopkeeper,$user,$pswrd){
		$em = $this->getDoctrine()->getManager();
		$accescontrol = new AccesControls();
		
		$accescontrol->setAlias($user);
		$accescontrol->setPswrd(md5($pswrd));
		$accescontrol->setShopkeeper($fkshopkeeper);
		
		$em->persist($accescontrol);
		$em->flush();
	}
	/**
	 * Check User 
	 */
	public function checkAction($username){
		$em = $this->getDoctrine()->getManager();
		$accesControl = $em ->getRepository('CviewPrincipalBundle:AccesControls')
						   	->findOneBy(array('Alias'=>$username));
		
		return (!$accesControl) ? true : false;
	}
	/**
	 * Check Login
	 */
	public function loginAction($username,$pswrd){
		$em = $this->getDoctrine()->getManager();
		$accesControl = $em ->getRepository('CviewPrincipalBundle:AccesControls')
							->findOneBy(array('Alias'=>$username,'Pswrd'=>md5($pswrd)));

		return (!$accesControl) ? false : true;
	}
	
	public function getAction($user){
		$em = $this->getDoctrine()->getManager();
		$acc = $em->getRepository('CviewPrincipalBundle:AccesControls')->findOneBy(array('Alias' => $user));
	
		$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->find($acc->getShopkeeper());
		return $shopkeeper->getCif();
	}
}