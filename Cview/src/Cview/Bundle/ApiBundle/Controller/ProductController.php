<?php

namespace Cview\Bundle\ApiBundle\Controller;
//use Symfony\Component\BrowserKit\Response;

use Symfony\Component\DependencyInjection\SimpleXMLElement;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Cview\Bundle\PrincipalBundle\Entity\AccesControls;
use Cview\Bundle\PrincipalBundle\Entity\Shopkeepers;
use Cview\Bundle\ApiBundle\Controller\ShopkeeperController;
use Cview\Bundle\ApiBundle\Tests\utils\SimpleImage;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cview\Bundle\PrincipalBundle\Entity\Products;
use Cview\Bundle\PrincipalBundle\Controller\ImageController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends Controller {
	/**
	 * @param unknown $type
	 * @param string $data
	 * @param array $options
	 */
	public function createForm($type, $data = null, array $options = array()) {
		// TODO: Auto-generated method stub

	}
	/**
	 * @param $status $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function controlAction($id,$status = null) {
		//Get Entity Manager
		$em = $this->getDoctrine()->getManager();

		//Get Shop
		$shop = $em->getRepository('CviewPrincipalBundle:Shops')->find($id);

		$products = $em->getRepository('CviewPrincipalBundle:Products')
				->findBy(array('Shop' => $shop->getId()));

		//Redirect dashboard
		return $this
				->render(
						'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
						array('part' => 'Products',
								'Shop_id' => $shop->getId(),
								'Products' => $products,
								'status' =>$status
		));
	}
	/**
	 * 
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function insertAction() {
		$em = $this->getDoctrine()->getManager();
		$shop_id = $_POST['shop_id'];
		if (isset($_POST['name']) && isset($_POST['description'])
				&& isset($_POST['price'])) {
			$name = $_POST['name'];
			$description = $_POST['description'];
			$price = $_POST['price'];
			$img = $_FILES['upfile']; 
			
			$image = new SimpleImage();
			$image->load($_FILES['upfile']['tmp_name']);
			$image->resizeToWidth(800);
			switch(strtolower($_FILES['upfile']['type']))
			{
				case 'image/jpeg':
					$image = imagecreatefromjpeg($_FILES['upfile']['tmp_name']);
					break;
				case 'image/png':
					$image = imagecreatefrompng($_FILES['upfile']['tmp_name']);
					break;
				case 'image/gif':
					$image = imagecreatefromgif($_FILES['upfile']['tmp_name']);
					break;
				default:
					exit('Unsupported type: '.$_FILES['upfile']['type']);
			}
			
			

			// Get current dimensions
			$old_width  = imagesx($image);
			$old_height = imagesy($image);
			
			$max_width = 800;
			$ratio = 800 / $old_width;
			$height = $old_height * $ratio;
			
			// Calculate the scaling we need to do to fit the image inside our frame
			//$scale      = min($max_width/$old_width, $max_height/$old_height);
			
			// Get the new dimensions
			$new_width  = $max_width;
			$new_height = $height;
			
			// Create new empty image
			$new = imagecreatetruecolor($new_width, $new_height);
			
			// Resize old image into new
			imagecopyresampled($new, $image,
			0, 0, 0, 0,
			$new_width, $new_height, $old_width, $old_height);
			$imagename = date("Ymdhis").rand();
			imagejpeg($new, '/home/hocklo/Development/MP13_PROJECTE/Symfony2/Cview/web/bundles/cview/img/products/'.$imagename.'.jpg');
			//$_FILES['upfile'] = $new;
			
			$shop_id = $_POST['shop_id'];
			if ($name != '' && $description != '' && $price != '') {
				//Si nos sube una imagen 
				if ($img != '') {
					/*
					$images = new ImageController();
					$images
							->upload_image(
									__DIR__
											. '/../../../../../web/bundles/cview/img/products',
									//'upfile');
									'upfile');*/
									
				}

				$shop = $em->getRepository('CviewPrincipalBundle:Shops')
						->find($shop_id);

				$product = new Products();
				$product->setName($name);
				$product->setDescription($description);
				$product->setPrice($price);
				$product->setImage($imagename.'.jpg');
				$product->setShop($shop);

				$em->persist($product);
				$em->flush();

				//Get Shop
				$shop = $em->getRepository('CviewPrincipalBundle:Shops')
						->find($shop_id);
				//Get Products
				$products = $em->getRepository('CviewPrincipalBundle:Products')
						->findBy(array('Shop' => $shop->getId()));
				$status = 11;
				return new RedirectResponse(
				$this->generateUrl('cview_principal_controlpanel_statusProduct',
						array('id' => $shop_id, 'status' => $status)));
			}
		}
		//Get Shop
		$shop = $em->getRepository('CviewPrincipalBundle:Shops')
				->find($shop_id);
		//Get Products
		$products = $em->getRepository('CviewPrincipalBundle:Products')
				->findBy(array('Shop' => $shop->getId()));
		//Error "campos en blanco"
		$status = 8;
		return new RedirectResponse(
				$this->generateUrl('cview_principal_controlpanel_statusProduct',
						array('id' => $shop_id, 'status' => $status)));
	}
	
	function deleteAction($id){
		$em = $this->getDoctrine()->getManager();
		
		$product = $em->getRepository('CviewPrincipalBundle:Products')->find($id);
		$shop_id = $product->getShop()->getId();
		$em->remove($product);
		$em->flush();
		//Producto elminado con exito
		$status = 12;
		return new RedirectResponse(
				$this->generateUrl('cview_principal_controlpanel_statusProduct',
						array('id' => $shop_id, 'status' => $status)));
	}
	
	function updateAction($id){
		$em = $this->getDoctrine()->getManager();
		
		$description = $_POST['description'];
		
		$product = $em->getRepository('CviewPrincipalBundle:Products')->find($id);
		$shop_id = $product->getShop()->getId();
		$product->setDescription($description);
		$em->persist($product);
		$em->flush();
		//Producto elminado con exito
		$status = 4;
		return new RedirectResponse(
				$this->generateUrl('cview_principal_controlpanel_statusProduct',
						array('id' => $shop_id, 'status' => $status)));
	}
	
	function allAction(){
		$em = $this->getDoctrine()->getManager();
		$q = $em -> createQuery('select p from Cview\Bundle\PrincipalBundle\Entity\Products p ');
		$products = $q->getArrayResult();
		
		if (!$products) {
			throw $this->createNotFoundException('Unable to find Products.');
		}
		
		$response = new Response(json_encode(array('products' => $products)));
		return $response;
	}
	
	function getAction($id){
		$em = $this->getDoctrine()->getManager();
		$q = $em -> createQuery('SELECT p FROM Cview\Bundle\PrincipalBundle\Entity\Products p
								 WHERE p.id = :id')->setParameter('id', $id);
		
		$products = $q->getArrayResult();
	
		if (!$products) {
			throw $this->createNotFoundException('Unable to find Products.');
		}
	
		$response = new Response(json_encode(array('products' => $products)));
		return $response;
	}
	function getForShopAction($shop){
		$conn = $this->get('database_connection');
		$products = $conn->fetchAll('SELECT * FROM Products WHERE Shop_id ='.$shop); 
	
		if (!$products) {
		   $response = new Response(json_encode(array('products' => '')));
		}else{
		   $response = new Response(json_encode(array('products' => $products)));
                }
		return $response;
	}
	
}

