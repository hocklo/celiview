<?php

namespace Cview\Bundle\ApiBundle\Controller;

//use Symfony\Component\BrowserKit\Response;

use Symfony\Component\Validator\Constraints\Length;

use Symfony\Component\DependencyInjection\SimpleXMLElement;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ValidationController extends Controller{
	/**
	 * Authenticate Shopkeeper
	 */
	public function authenticateAction(){
		$user = $_POST["usr_id"];
		$pswrd = $_POST["pswrd_id"];
		$acces_control  = new AccesControlController();
		$acces_control->setContainer($this->container);
		if($acces_control->loginAction($user,$pswrd)){
			$cif = $acces_control->getAction($user);
			//encode cif
			//random aleatori de seguretat
			$cif = base64_encode(substr($cif,0,4).rand(1000,9999).substr($cif,4,8));
			setcookie("Shopkeeper", $cif , time()+3600);  /* expira en una hora */
			setcookie("Shopkeeper", $cif , time()+3600);  /* expira en una hora */
			//Redirect dashboard
			return new RedirectResponse(
					$this->generateUrl(
							'cview_principal_controlpanel',array('part'=>'Tiendas')
							)
					);
		}else{
			$error = 'Nombre de usuario o contraseña incorrectos.';
			//Comprobar Coockie
			$loged = (isset($_COOKIE['Shopkeeper'])) ? true : false;

			return $this->render(
					'CviewPrincipalBundle:Default:index.html.twig',
						array('error' => $error,'loged' => $loged)
			);
		}
    }//End of authenticateAction
    
    public function calCifAction($cif){
    	//Comprobar que $cif no llega como nulo
    		if($cif==null || $cif==""){
    			return	$validez_cif = false;
    		}
    	//Comprobar que $cif tiene 9 caracteres
    		if (strlen($cif) !== 9){
    			return	$validez_cif = false;
    		}
    	//Comprobar que el primer caracter de $cif es una letra de $inicio
    		$inicio = "ABCDEFGHQSKLM";
    		if (strpos ($inicio,substr($cif,0,1)) === false){
    			return	$validez_cif = false;
    		}else{
    			$c=0;
    			for ($i=2; $i <=6; $i= $i+2){
    				$n = substr($cif,$i,1);
    				$c += $n;
    			}
    			$r=0;
    			for ($s=1; $s <=7; $s= $s+2){
    				$d =substr($cif,$s,1);
    				$d *= 2;
    				if (strlen ($d)==2){
    					$d = substr ($d,0,1)+ substr ($d,1,1);
    				}
    				$r += $d;
    			}
    			$dc=$c+$r;
    			$dc = 10 - substr ($dc,strlen($dc)-1,1);
    			if (substr ($cif,8,1) === substr ($dc,strlen($dc)-1,1)){
    				return $validez_cif =true;
    			}
    			else {return $validez_cif = false;
    			}
    		}
    }//End of calCifAction
    
    public function registerAction($cif,$name,$lastname,$email){
    	$valCif = self::calCifAction($cif);
    	if($valCif){
    		$shopkeeper = new ShopkeeperController();
    		$shopkeeper->setContainer($this->container);
			if($shopkeeper->checkAction($cif)){
				$shopkeeper->insertAction($cif, $name, $lastname, $email);
				//Generar usuari i contrasenya : 
					$username = self::genUsrAction($name, $lastname);
					$pswrd = self::genPswrdAction(10);
				//El nom d'usuari no existeix
					$acces_control = new AccesControlController();
					$acces_control->setContainer($this->container);
					$fkshopkeeper = $shopkeeper->getforcifAction($cif);
					$acces_control->insertAction($fkshopkeeper, $username, $pswrd);
				//Crear plantilla per el missatge
					$message = \Swift_Message::newInstance()
						->setSubject('Cview - Registro de usuario')
						->setFrom('ecollvila@gmail.com')
						->setTo($email)
						->setContentType("text/html") //Cuerpo del mail en html
						->setBody(
								$this->renderView(
									'CviewPrincipalBundle:Email:Email.html.twig',
									array('name' => $name, 'username' => $username, 'pswrd' => $pswrd)
								)//End this
						);//End setBody
				$this->get('mailer')->send($message);
				$response = new Response("Registro Completado.\nSe te ha enviado un correo electronico con tus datos de acceso.");
				return $response;
			}else{
				$response = new Response("El Cif introducido ya existe.");
				return $response;
			}
    	}else{
    		$response = new Response("El Cif introducido no es valido.");
    		return $response;
    	}
    }
    /*
     * Generate Password
     */
    public function genPswrdAction($length){
    	$exp_reg="[^A-Z0-9]";    
    	return substr(	preg_replace($exp_reg, "", md5(rand())) .
       					preg_replace($exp_reg, "", md5(rand())) .
       					preg_replace($exp_reg, "", md5(rand())),
       					0, $length);
    }
    /*
     * Generate User
     */
    public function genUsrAction($name, $lastname){
    	$username = substr($name,0,1).substr($lastname,0,1)."cv".self::genPswrdAction(6);
    	$conn = $this->get('database_connection');
    	$acces_control = new AccesControlController();
    	$acces_control->setContainer($this->container);
    	while(true){
	    	if(!$acces_control->checkAction($username)){
	    		$username = substr($name,0,1).substr($lastname,0,1)."cv".self::genPswrdAction(6);
	    	}else{
	    		break;
	    	}
    	}
    	return $username;	
    }
    
    public function logoutAction(){
    	setcookie('Shopkeeper','',time() - 7200);
    	
    	return $this->render(
    			'CviewPrincipalBundle:Default:index.html.twig',
    			array('loged' => false)
    	);
    }
}