<?php

namespace Cview\Bundle\ApiBundle\Controller;

//use Symfony\Component\BrowserKit\Response;

use Cview\Bundle\PrincipalBundle\Entity\Shopkeepers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Cview\Bundle\PrincipalBundle\Entity\Shops;
use Cview\Bundle\PrincipalBundle\Entity\Provinces;
use Cview\Bundle\PrincipalBundle\Entity\Cities;
use Cview\Bundle\ApiBundle\Controller\ShopkeeperController;
use Symfony\Component\Validator\Constraints\Length;
use Cview\Bundle\ApiBundle\Tests\utils\SimpleImage;


class ShopController extends Controller{
	/**
	 * Show All Shopkeepers
	 */
	public function allAction(){
		$em = $this->getDoctrine()->getManager();
		$q = $em -> createQuery('select s from Cview\Bundle\PrincipalBundle\Entity\Shops s');
		$shops = $q->getArrayResult();
	
		if (!$shops) {
			throw $this->createNotFoundException('Unable to find Shops.');
		}
	
		$response = new Response(json_encode(array('shops' => $shops)));
		return $response;
	}
	
	/**
	 * Show Shopkeeper for id
	 */
	public function getAction($id){
	
		$conn = $this->get('database_connection');
		$shop = $conn->fetchAll('SELECT * FROM Shops WHERE id='.$id);
	
		if (!$shops) {
			throw $this->createNotFoundException('Unable to find Shops.');
		}
	
		$response = new Response(json_encode(array('shop' => $shop)));
		return $response;
	}
	/**
	 * Show Shopkeeper for zone
	 */
	public function getZoneAction($latitude_de, $latitude_bd, $longitude_de, $longitude_bd){
		$conn = $this->get('database_connection');
		$shops = $conn->fetchAll('SELECT * FROM Shops 
				WHERE latitude <('.$latitude_de.'+0.000000001) AND latitude >('.$latitude_bd.' + 0.000000001)
				AND longitude <('.$longitude_de.'+0.000000001) AND longitude >('.$longitude_bd.' +0.000000001)');
		
		if (!$shops) {
			throw $this->createNotFoundException('Unable to find Shops.');
		}
	
		$response = new Response(json_encode(array('shops' => $shops)));
		return $response;
	}
	/**
	 * @param $status $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function controlAction($id,$status = null) {
		//Get Entity Manager
		$em = $this->getDoctrine()->getManager();
		
		if($status!=13){
			//Get Shop
			$shop = $em->getRepository('CviewPrincipalBundle:Shops')->find($id);
	
			$shopkeeper = $shop->getShopkeeper();
		}else{
			$cif = $_COOKIE['Shopkeeper'];
			$cif= base64_decode($cif);
			$cif = substr($cif, 0,4).substr($cif,8,12);
			$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
		}
		//Check myShops
			$insertShops = ($shopkeeper->getMyShops() < $shopkeeper->getMaxShops())?true:false;
			
			//Return MyShops
			$myShops = $em->getRepository('CviewPrincipalBundle:Shops')->findBy(array('Shopkeeper' => $shopkeeper->getId()));
			
			return $this->render(
					'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
					array('part' => 'Tiendas','insertShop' => $insertShops, 'status' => $status, 'myShops' => $myShops));
	}
	/**
	 * GetName Shopkeeper for id
	 * @return username
	 */
	public function getNameAction(){
		$loged = (isset($_COOKIE['Shopkeeper'])) ? true : false;
		if($loged){
			$cif = $_COOKIE['Shopkeeper'];
			$em = $this->getDoctrine()->getManager();
			$cif= base64_decode($cif);
			$cif = substr($cif, 0,4).substr($cif,9,12);
			$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif',$cif));

			return new Response($shopkeeper->getName());
		}
	}
	/**
	 * Insert Shop in Database
	 */
	public function insertAction(){
		$em = $this->getDoctrine()->getManager();
		if(isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['direction']) && isset($_POST['description'])){
			$name = $_POST['name'];
			$phone = $_POST['phone'];
			$direction = $_POST['direction'];
			
			if($name!='' && $phone !='' && $direction !=''){
				if($name!=''){
					if(self::validatePhone($phone)){
						$lat = $_POST['lat']; $lon = $_POST['lng'];
						$city = $_POST['city']; $province = $_POST['province'];
						$description = $_POST['description'];
						
						$pro = $em->getRepository('CviewPrincipalBundle:Provinces')->findOneBy(array('Name' => $province));
						if(!$pro){
							$pro = new Provinces();
							$pro->setName($province);
							$em->persist($pro);
							$em->flush();
						}
						
						$cit = $em->getRepository('CviewPrincipalBundle:Cities')
							->findOneBy(
								array('Name' => $city, 'Province' => $pro)
						);
						
						if(!$cit){
							$cit = new Cities();
							$cit->setName($city);
							$cit->setProvince($pro);
							$em->persist($cit);
							$em->flush();
						}
						
						if(isset($_FILES['upfile']) && $_FILES['upfile']['name']!=''){
						$img = $_FILES['upfile'];
						$image = new SimpleImage();
						$image->load($_FILES['upfile']['tmp_name']);
						$image->resizeToWidth(800);
						switch(strtolower($_FILES['upfile']['type']))
						{
							case 'image/jpeg':
								$image = imagecreatefromjpeg($_FILES['upfile']['tmp_name']);
								break;
							case 'image/png':
								$image = imagecreatefrompng($_FILES['upfile']['tmp_name']);
								break;
							case 'image/gif':
								$image = imagecreatefromgif($_FILES['upfile']['tmp_name']);
								break;
							default:
								exit('Unsupported type: '.$_FILES['upfile']['type']);
						}
							
							
						
						// Get current dimensions
						$old_width  = imagesx($image);
						$old_height = imagesy($image);
							
						$max_width = 800;
						$ratio = 800 / $old_width;
						$height = $old_height * $ratio;
							
						// Calculate the scaling we need to do to fit the image inside our frame
						//$scale      = min($max_width/$old_width, $max_height/$old_height);
							
						// Get the new dimensions
						$new_width  = $max_width;
						$new_height = $height;
							
						// Create new empty image
						$new = imagecreatetruecolor($new_width, $new_height);
							
						// Resize old image into new
						imagecopyresampled($new, $image,
						0, 0, 0, 0,
						$new_width, $new_height, $old_width, $old_height);
						$imgname= date("Ymdhis").rand().'.jpg';
						imagejpeg($new, '/home/hocklo/Development/MP13_PROJECTE/Symfony2/Cview/web/bundles/cview/img/shops/'.$imgname);
						
						}else{
							$imgname = "shop_not_found.png";
						}
						$cif = $_COOKIE['Shopkeeper'];
    					$cif= base64_decode($cif);
    					$cif = substr($cif, 0,4).substr($cif,8,12);
						$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
						
						$shop = new Shops();
						$shop->setName($name);
						$shop->setPhone($phone);
						$shop->setDescription($description);
						$shop->setDirection($direction);
						$shop->setImage($imgname);
						$shop->setLatitude($lat); $shop->setLongitude($lon);
						$shop->setCity($cit);
						$shop->setShopkeeper($shopkeeper);
						$em->persist($shop);
						$em->flush();
						
						//Increment Shopkeeper Shops after Reg
						$shopkeeper->setMyShops($shopkeeper->getMyShops()+1);
						$em->persist($shopkeeper);
						$em->flush();
						
						//Check myShops
						$insertShops = ($shopkeeper->getMyShops() < $shopkeeper->getMaxShops())?true:false;
						
						//Return MyShops
						$myShops = $em->getRepository('CviewPrincipalBundle:Shops')->findBy(array('Shopkeeper' => $shopkeeper->getId()));
						
						
						$status = 4;
						return new RedirectResponse(
								$this->generateUrl('cview_principal_controlpanel',
										array('part'=>'Tiendas')));
					}else{
						$status = 10;
					}
				}else{
					$status = 9;
					return $this->render(
							'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
							array('part' => 'Tiendas','insertShop' => $insertShops, 'status' => $status));
				}
			}else{$status = 8;}
		}
		return $this->render(
				'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
				array('part' => 'Tiendas','insertShop' => true, 'status' => $status));
	}
	
	public function updateAction($id){
		$em = $this->getDoctrine()->getManager();
		
		if(isset($_POST[$id.'name']) && isset($_POST[$id.'phone']) && isset($_POST[$id.'direction']) && isset($_POST[$id.'description'])){
			$name = $_POST[$id.'name'];
			$phone = $_POST[$id.'phone'];
			$direction = $_POST[$id.'direction'];
			$description = $_POST[$id.'description'];
			if($name!='' && $phone!='' && $direction!=''){
				$shop = $em->getRepository('CviewPrincipalBundle:Shops')->find($id);
				$shop->setName($name);
				$shop->setPhone($phone);
				$shop->setDirection($direction);
				$shop->setDescription($description);
				
				$em->persist($shop);
				$em->flush();
				
				$status = 4;
			}else{
				$status = 8;	
			}
		}else{
			//No dejes campos en blanco
			$status = 8;
		}
		return new RedirectResponse(
				$this->generateUrl('cview_principal_controlpanel_statusShop',
						array('id' => $id, 'status' => $status)));
	}
	public function deleteAction($id){
		$em = $this->getDoctrine()->getManager();
		$shop = $em->getRepository('CviewPrincipalBundle:Shops')->find($id);
		
		$shopkeeper = $shop->getShopkeeper();
		
		$em->remove($shop);
		$em->flush();
		
		$shopkeeper->setMyShops($shopkeeper->getMyShops()-1);
		$em->persist($shopkeeper);
		$em->flush();
		
		$status = 13;
		
		return new RedirectResponse(
				$this->generateUrl('cview_principal_controlpanel_statusShop',
						array('id' => $id, 'status' => $status)));
	}

	/**
	 * Funcion Regular para validar el telefono(Spain)
	 * @param Integer $telefono
	 * @return number
	 */
	function validatePhone($phone)
	{
		return preg_match('/^[9|8|6|7][0-9]{8}$/', $phone);
	}
	
	public function getCurrent(){
		$cif = $_COOKIE['Shopkeeper'];
		$em = $this->getDoctrine()->getManager();
		$cif= base64_decode($cif);
		$cif = substr($cif, 0,4).substr($cif,9,12);
		$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('cif',$cif));
	
		return $shopkeeper;
	}
	
}