<?php

namespace Cview\Bundle\PrincipalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SitesController extends Controller
{
    public function staticAction($page){
        return $this->render(':static_pages:'.$page.'html.twig');
    }
}
