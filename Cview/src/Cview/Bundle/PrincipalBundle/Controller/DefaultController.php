<?php
namespace Cview\Bundle\PrincipalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    public function indexAction(){
    	//Comprobar Coockie
    	$loged = (isset($_COOKIE['Shopkeeper'])) ? true : false;
  
        return $this->render('CviewPrincipalBundle:Default:index.html.twig',array('loged' => $loged));
    }
    public function controlAction($part){
    	//LLEGIR COOCKIE !
    	if(isset($_COOKIE['Shopkeeper'])){
    		$cif = $_COOKIE['Shopkeeper'];
    		$cif= base64_decode($cif);
    		$cif = substr($cif, 0,4).substr($cif,8,12);
    		
    		if($part == 'Perfil'){
    		return self::getInfo($cif);
    			
    		}
    		$em = $this->getDoctrine()->getManager();
    		$shopkeeper = $em->getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
    		
    		//Check myShops
    		$insertShops = ($shopkeeper->getMyShops() < $shopkeeper->getMaxShops())?true:false;
    		
    		//Return MyShops
    		$myShops = $em->getRepository('CviewPrincipalBundle:Shops')->findBy(array('Shopkeeper' => $shopkeeper->getId()));
    		
    		return $this->render(
    			'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
    			array('cif' => $cif,'part' => $part, 'insertShop' => $insertShops, 'myShops' => $myShops)
    		);
    	}else{
    		return new RedirectResponse($this->generateUrl('cview_principal_homepage'));
    	}
    }  
    public function getInfo($cif){
    	$em = $this -> getDoctrine()->getManager();
    	
    	$shopkeeper = $em -> getRepository('CviewPrincipalBundle:Shopkeepers')->findOneBy(array('Cif' => $cif));
    	$acc = $em -> getRepository('CviewPrincipalBundle:AccesControls')->findOneBy(array('Shopkeeper' => $shopkeeper));
    	$name = $shopkeeper->getName()." ".$shopkeeper->getLastName();

    	return $this->render(
    			'CviewPrincipalBundle:ShopControl:controlpanel.html.twig',
    			array('cif' => $cif, 'part' => 'Perfil', 'name' => $name,
    				  'email' => $shopkeeper->getEmail(), 'Alias' => $acc->getAlias()));
    }
    
    public function updateAction(){
    	$cif = $_POST['inputShopkeeperCif'];
    	$email = $_POST['inputShopkeeperEmail'];
    }
}
