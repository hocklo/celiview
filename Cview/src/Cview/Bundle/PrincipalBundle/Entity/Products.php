<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Brands") */
    private $Brand;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text")
     */
    private $Description;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=255)
     */
    private $Image;

    /**
     * @var float
     *
     * @ORM\Column(name="Price", type="float")
     */
    private $Price;
	
    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Shops")
     */
    private $Shop;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Brand
     *
     * @param string $brand
     * @return Products
     */
    public function setBrand(\Cview\Bundle\PrincipalBundle\Entity\Brands $brand)
    {
        $this->Brand = $brand;
    
        return $this;
    }

    /**
     * Get Brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->Brand;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Products
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return Products
     */
    public function setDescription($description)
    {
        $this->Description = $description;
    
        return $this;
    }

    /**
     * Get Description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * Set Image
     *
     * @param string $image
     * @return Products
     */
    public function setImage($image)
    {
        $this->Image = $image;
    
        return $this;
    }

    /**
     * Get Image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->Image;
    }

    /**
     * Set Price
     *
     * @param float $price
     * @return Products
     */
    public function setPrice($price)
    {
        $this->Price = $price;
    
        return $this;
    }

    /**
     * Get Price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->Price;
    }
    /**
     * 
     * @param \Cview\Bundle\PrincipalBundle\Entity\Shops $shop
     * @return \Cview\Bundle\PrincipalBundle\Entity\Products
     */
    public function setShop(\Cview\Bundle\PrincipalBundle\Entity\Shops $shop){
    	$this->Shop = $shop;
    	
    	return $this;
    }
    /**
     * Get Shop
     * @return string
     * 
     */
    public function getShop(){
    	return $this->Shop;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
