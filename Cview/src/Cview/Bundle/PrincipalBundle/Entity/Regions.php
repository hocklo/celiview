<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regions
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Regions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Countries")
     *
     */
    private $Country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set Country
     *
     * @param string $country
     * @return Regions
     */
    public function setCountry(\Cview\Bundle\PrincipalBundle\Entity\Countries $country)
    {
    	$this->Country = $country;
    
    	return $this;
    }

    /**
     * Get Country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Regions
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }
  
    public function __toString()
    {
    	return $this->getName();
    }
}
