<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shopkeepers
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Shopkeepers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Cif", type="string", length=9)
     */
    private $Cif;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=255)
     */
    private $LastName;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255)
     */
    private $Email;
    /**
     * @var integer
     * 
     * @ORM\Column(name="myShops", type="integer")
     */
	private $myShops;
	/**
	 * @var integer
	 * 
	 * @ORM\Column(name="maxShops", type="integer")
	 */
	private $maxShops;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Cif
     *
     * @param string $cif
     * @return Shopkeepers
     */
    public function setCif($cif)
    {
        $this->Cif = $cif;
    
        return $this;
    }

    /**
     * Get Cif
     *
     * @return string 
     */
    public function getCif()
    {
        return $this->Cif;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Shopkeepers
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * Set LastName
     *
     * @param string $lastName
     * @return Shopkeepers
     */
    public function setLastName($lastName)
    {
        $this->LastName = $lastName;
    
        return $this;
    }

    /**
     * Get LastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     * Set Email
     *
     * @param string $email
     * @return Shopkeepers
     */
    public function setEmail($email)
    {
        $this->Email = $email;
    
        return $this;
    }

    /**
     * Get Email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->Email;
    }
    /**
     * 
     * @param integer $myShops
     */
    public function setMyShops($myShops){
    	$this->myShops = $myShops;
    }
    /**
     * 
     * @return integer
     */
    public function getMyShops(){
    	return $this->myShops;
    }
    /**
     * 
     * @param integer $maxShops
     */
    public function setMaxShops($maxShops){
    	$this->maxShops = $maxShops;
    }
    /**
     * 
     * @return integer
     */
    public function getMaxShops(){
    	return $this->maxShops;
    }
    /**
     * 
     * @return string
     */
    public function __toString()
    {
    	return $this->getName();
    }
    
    
}
