<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brands
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Brands
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Logo", type="string", length=255)
     */
    private $Logo;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text")
     */
    private $Description;

    /**
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Countries")
     *
     */
    private $Country;

    /**
     * @var string
     *
     * @ORM\Column(name="Thumbnails", type="string", length=255)
     */
    private $Thumbnails;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Logo
     *
     * @param string $logo
     * @return Brands
     */
    public function setLogo($logo)
    {
        $this->Logo = $logo;
    
        return $this;
    }

    /**
     * Get Logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->Logo;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Brands
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return Brands
     */
    public function setDescription($description)
    {
        $this->Description = $description;
    
        return $this;
    }

    /**
     * Get Description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * Set Country
     *
     * @param string $country
     * @return Brands
     */
    public function setCountry(\Cview\Bundle\PrincipalBundle\Entity\Countries $country)
    {
        $this->Country = $country;
    
        return $this;
    }

    /**
     * Get Country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Set Thumbnails
     *
     * @param string $thumbnails
     * @return Brands
     */
    public function setThumbnails($thumbnails)
    {
        $this->Thumbnails = $thumbnails;
    
        return $this;
    }

    /**
     * Get Thumbnails
     *
     * @return string 
     */
    public function getThumbnails()
    {
        return $this->Thumbnails;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
