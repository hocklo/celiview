<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccesControls
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AccesControls
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Shopkeepers")
     */
    private $Shopkeeper;

    /**
     * @var string
     *
     * @ORM\Column(name="Alias", type="string", length=10)
     */
    private $Alias;

    /**
     * @var string
     *
     * @ORM\Column(name="Pswrd", type="string", length=255)
     */
    private $Pswrd;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   /**
    * 
    * @param \Cview\Bundle\PrincipalBundle\Entity\Shopkeepers $shopkeeper
    * @return \Cview\Bundle\PrincipalBundle\Entity\AccesControls
    */
    public function setShopkeeper(\Cview\Bundle\PrincipalBundle\Entity\Shopkeepers $shopkeeper)
    {
        $this->Shopkeeper = $shopkeeper;
    
        return $this;
    }

    /**
     * 
     * @return \Cview\Bundle\PrincipalBundle\Entity\Shopkeepers
     */
    public function getShopkeeper()
    {
        return $this->Shopkeeper;
    }

    /**
     * Set Alias
     *
     * @param string $alias
     * @return AccesControls
     */
    public function setAlias($alias)
    {
        $this->Alias = $alias;
    
        return $this;
    }

    /**
     * Get Alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->Alias;
    }

    /**
     * Set Pswrd
     *
     * @param string $pswrd
     * @return AccesControls
     */
    public function setPswrd($pswrd)
    {
        $this->Pswrd = $pswrd;
    
        return $this;
    }

    /**
     * Get Pswrd
     *
     * @return string 
     */
    public function getPswrd()
    {
        return $this->Pswrd;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
