<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cities
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Cities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Countries")
     *
     */
    private $Country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Regions")
     */
    private $Region;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Provinces")
     */
    private $Province;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Country
     *
     * @param \Cview\Bundle\PrincipalBundle\Entity\Countries $country
     * @return \Cview\Bundle\PrincipalBundle\Entity\Cities
     */
    public function setCountry(\Cview\Bundle\PrincipalBundle\Entity\Countries $country)
    {
    	$this->Country = $country;
    
    	return $this;
    }

    /**
     * Get Country
     *
     * @return \Cview\Bundle\PrincipalBundle\Entity\Countries
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Set Region
     *
     * @param \Cview\Bundle\PrincipalBundle\Entity\Regions $region
     * @return \Cview\Bundle\PrincipalBundle\Entity\Cities
     */
    public function setRegion(\Cview\Bundle\PrincipalBundle\Entity\Regions $region)
    {
        $this->Region = $region;
    
        return $this;
    }

    /**
     * Get Region
     *
     * @return \Cview\Bundle\PrincipalBundle\Entity\Regions
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * Set Province
     *
     * @param \Cview\Bundle\PrincipalBundle\Entity\Provinces $province
     * @return \Cview\Bundle\PrincipalBundle\Entity\Cities
     */
    public function setProvince(\Cview\Bundle\PrincipalBundle\Entity\Provinces $province)
    {
        $this->Province = $province;
    
        return $this;
    }

    /**
     * Get Province
     *
     * @return \Cview\Bundle\PrincipalBundle\Entity\Provinces
     */
    public function getProvince()
    {
        return $this->Province;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Cities
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
