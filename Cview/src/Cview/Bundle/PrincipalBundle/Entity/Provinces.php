<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Provinces
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Provinces
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Countries")
     *
     */
    private $Country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Regions")
     */
    private $Region;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Country
     *
     * @param string $country
     * @return Provinces
     */
    public function setCountry(\Cview\Bundle\PrincipalBundle\Entity\Countries $country)
    {
    	$this->Country = $country;
    
    	return $this;
    }

    /**
     * Get Country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * Set Region
     *
     * @param string $region
     * @return Provinces
     */
    public function setRegion(\Cview\Bundle\PrincipalBundle\Entity\Regions $region)
    {
        $this->Region = $region;
    
        return $this;
    }

    /**
     * Get Region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Provinces
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
