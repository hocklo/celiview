<?php

namespace Cview\Bundle\PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Translation\Tests\String;

/**
 * Shops
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Shops
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Shopkeepers")
     */
    private $Shopkeeper;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255)
     */
    private $Name;

    /**
     * @var string
     *
     * @ORM\Column(name="Phone", type="string", length=20)
     */
    private $Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text")
     */
    private $Description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Direction", type="string", length=200)
     */
    private $Direction;
    /**
     *
     * @ORM\ManyToOne(targetEntity="Cview\Bundle\PrincipalBundle\Entity\Cities")
     */
    private $City;

    /**
     * @var float
     *
     * @ORM\Column(name="Longitude", type="float")
     */
    private $Longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="Latitude", type="float")
     */
    private $Latitude;

    /**
     * 
     * @var string
     * @ORM\Column(name="Image", type="string", length=200)
     */
	private $Image;
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * 
	 * @param \Cview\Bundle\PrincipalBundle\Entity\Shopkeepers $shopkeeper
	 * @return \Cview\Bundle\PrincipalBundle\Entity\Shops
	 */
    public function setShopkeeper(\Cview\Bundle\PrincipalBundle\Entity\Shopkeepers $shopkeeper)
    {
        $this->Shopkeeper = $shopkeeper;
    
        return $this;
    }

    /**
     * Get Shopkeeper
     *
     * @return \Cview\Bundle\PrincipalBundle\Entity\Shopkeepers
     */
    public function getShopkeeper()
    {
        return $this->Shopkeeper;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Shops
     */
    public function setName($name)
    {
        $this->Name = $name;
    
        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * Set Phone
     *
     * @param string $phone
     * @return Shops
     */
    public function setPhone($phone)
    {
        $this->Phone = $phone;
    
        return $this;
    }

    /**
     * Get Phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * Set Description
	 *
     * @param string $description
     * @return \Cview\Bundle\PrincipalBundle\Entity\Shops
     */
    public function setDescription($description)
    {
        $this->Description = $description;
    
        return $this;
    }

    /**
     * Get Description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->Description;
    }
    
    /**
     * Set Direction
     *
     * @param string $direction
     * @return Shops
     */
    public function setDirection($direction)
    {
    	$this->Direction = $direction;
    
    	return $this;
    }
    
    /**
     * Get Direction
     *
     * @return string
     */
    public function getDirection()
    {
    	return $this->Direction;
    }
    
    /**
     * Set City
     *
     * @param \Cview\Bundle\PrincipalBundle\Entity\Cities $city
     * @return \Cview\Bundle\PrincipalBundle\Entity\Shops
     */
    public function setCity(\Cview\Bundle\PrincipalBundle\Entity\Cities $city)
    {
        $this->City = $city;
    
        return $this;
    }

    /**
     * Get City
     *
     * @return \Cview\Bundle\PrincipalBundle\Entity\Cities
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * Set Longitude
     *
     * @param float $longitude
     * @return Shops
     */
    public function setLongitude($longitude)
    {
        $this->Longitude = $longitude;
    
        return $this;
    }

    /**
     * Get Longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->Longitude;
    }

    /**
     * Set Latitude
     *
     * @param float $latitude
     * @return Shops
     */
    public function setLatitude($latitude)
    {
        $this->Latitude = $latitude;
    
        return $this;
    }

    /**
     * Get Latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->Latitude;
    }
    
    /**
     * Set Image
     *
     * @param string $image
     * @return Shops
     */
    public function setImage($image)
    {
    	$this->Image = $image;
    
    	return $this;
    }
    
    /**
     * Get Image
     *
     * @return string
     */
    public function getImage()
    {
    	return $this->Image;
    }
    
    public function __toString()
    {
    	return $this->getName();
    }
}
